package com.github.axet.smsgate.widgets;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.util.AttributeSet;

import com.github.axet.smsgate.app.Storage;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NameFormatPreferenceCompat extends com.github.axet.androidlibrary.preferences.NameFormatPreferenceCompat {
    public NameFormatPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public NameFormatPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public NameFormatPreferenceCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NameFormatPreferenceCompat(Context context) {
        super(context);
    }

    @Override
    public void create() {
        super.create();
        if (!StoragePathPreferenceCompat.isVisible(getContext()))
            setVisible(false);
    }

    @Override
    public String getFormatted(String str) {
        String n = getPredefined(str);
        if (n != null)
            return n;
        return Storage.getFormatted(str, new Storage.SMSMessage(1493561080000l, "IN", "+1333445566", "Name Contact", "Message text"));
    }
}
