package com.github.axet.smsgate.providers;

import android.content.Context;
import android.os.Build;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;

import com.github.axet.smsgate.mediatek.TelephonyManagerMT;

import java.util.ArrayList;
import java.util.List;

public class SIM {
    public static final String TAG = SIM.class.getSimpleName();

    List<Integer> sims = new ArrayList<>();
    TelephonyManagerMT mt;
    SubscriptionManager ss;

    public SIM(Context context) {
        if (Build.VERSION.SDK_INT >= 22) {
            ss = SubscriptionManager.from(context); // READ_PHONE_STATE
            List<SubscriptionInfo> ll = ss.getActiveSubscriptionInfoList();
            if (ll == null)
                return;
            for (SubscriptionInfo s : ll)
                sims.add(s.getSubscriptionId());
            return;
        }

        try {
            mt = new TelephonyManagerMT(context);
            for (int i = 0; i < 5; i++) {
                if (mt.getSimState(i) != 0)
                    sims.add(i);
            }
        } catch (Throwable ignore) {
        }
    }

    public int getCount() {
        return sims.size();
    }

    public int getSimID(int i) {
        return sims.get(i);
    }

    public int findID(int id) {
        for (int i = 0; i < sims.size(); i++) {
            if (sims.get(i) == id)
                return i;
        }
        return -1;
    }

    public String getSerial(int i) {
        if (Build.VERSION.SDK_INT >= 22) {
            int id = sims.get(i);
            SubscriptionInfo s = ss.getActiveSubscriptionInfo(id);
            return s.getIccId();
        }
        int id = sims.get(i);
        return mt.getSimSerialNumber(id);
    }

    public String getOperatorName(int i) {
        if (Build.VERSION.SDK_INT >= 22) {
            int id = sims.get(i);
            SubscriptionInfo s = ss.getActiveSubscriptionInfo(id);
            return s.getDisplayName().toString();
        }
        int id = sims.get(i);
        return mt.getSimOperatorName(id);
    }

    public String getOperatorCode(int i) {
        if (Build.VERSION.SDK_INT >= 22) {
            int id = sims.get(i);
            SubscriptionInfo s = ss.getActiveSubscriptionInfo(id);
            return s.getCarrierName().toString();
        }
        int id = sims.get(i);
        return mt.getSimOperator(id);
    }
}
