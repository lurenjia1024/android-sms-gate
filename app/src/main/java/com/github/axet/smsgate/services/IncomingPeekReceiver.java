/* Copyright (c) 2009 Christoph Studer <chstuder@gmail.com>
 * Copyright (c) 2010 Jan Berkel <jan.berkel@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.axet.smsgate.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.util.Log;

import com.github.axet.smsgate.app.Storage;
import com.zegoggles.smssync.App;
import com.zegoggles.smssync.activity.SMSGateFragment;
import com.zegoggles.smssync.mail.DataType;
import com.zegoggles.smssync.service.BackupType;
import com.zegoggles.smssync.service.SmsBackupService;
import com.zegoggles.smssync.service.state.BackupState;
import com.zegoggles.smssync.service.state.SmsSyncState;

import static com.zegoggles.smssync.App.LOCAL_LOGV;
import static com.zegoggles.smssync.App.TAG;

public class IncomingPeekReceiver extends BroadcastReceiver { // sms received notification
    @Override
    public void onReceive(Context context, Intent intent) {
        if (LOCAL_LOGV) Log.v(TAG, "onReceive(" + context + "," + intent + ")");
        String a = intent.getAction();
        if (a == null)
            return;
        if (a.equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION))
            incomingSMS(context);
    }

    public void incomingSMS(Context context) {
        SMSGateFragment.checkPermissions(context);
        FirebaseService.incoming(context);
        long sms = SmsBackupService.scheduleIncomingBackup(context);
        Runnable done = null;
        if (sms == -1) { // only post progress status if here is no IMAP backups (to prevent status jaggling)
            done = new Runnable() {
                @Override
                public void run() {
                    App.bus.post(new BackupState(SmsSyncState.INITIAL, 0, 0, BackupType.MANUAL, DataType.SMS, null));
                }
            };
        }
        Storage.incoming(context, done);
    }
}
