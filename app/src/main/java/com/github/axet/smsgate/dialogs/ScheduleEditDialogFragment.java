package com.github.axet.smsgate.dialogs;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.ScheduleSMS;
import com.github.axet.smsgate.app.ScheduleTime;
import com.github.axet.smsgate.providers.SIM;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ScheduleEditDialogFragment extends DialogFragment {
    View v;

    ScheduleResult result;

    SIM sm;

    TextView phone;
    TextView message;
    CheckBox hide;
    Spinner repeat;
    Spinner sim;
    TextView date;
    TextView time;
    TextView next;

    public static class SIMItem {
        public String name;
        public int id;

        public SIMItem(String n, int i) {
            this.name = n;
            this.id = i;
        }

        public String toString() {
            return name;
        }
    }

    public static class ScheduleResult implements DialogInterface {
        public boolean save = false;
        public boolean delete = false;
        public int pos;
        public ScheduleSMS schedule;

        @Override
        public void cancel() {
        }

        @Override
        public void dismiss() {
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Activity a = getActivity();
        if (a instanceof DialogInterface.OnDismissListener)
            ((DialogInterface.OnDismissListener) a).onDismiss(result);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity())
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                save();
                                result.save = true;
                                dialog.dismiss();
                            }
                        }
                )
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .setTitle("Add Schedule")
                .setView(createView(LayoutInflater.from(getContext()), null, savedInstanceState));

        final int pos = getArguments().getInt("pos");
        if (pos != -1) {
            b.setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            final AlertDialog d = b.create();

            d.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button b = d.getButton(AlertDialog.BUTTON_NEUTRAL);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle("Delete ScheduleSMS?");
                            builder.setMessage("Are you sure?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    result.delete = true;
                                    dialog.cancel();
                                    dismiss();
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.show();
                        }
                    });
                }
            });
            return d;
        } else {
            final AlertDialog d = b.create();
            return d;
        }
    }

    @Nullable
    @Override
    public View getView() {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return null;
    }

    public View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.schedule_edit, container, false);

        sm = SMSApplication.from(getContext()).getSIM();

        result = new ScheduleResult();
        String json = getArguments().getString("sms");
        if (json == null || json.isEmpty())
            result.schedule = new ScheduleSMS(getContext());
        else
            result.schedule = new ScheduleSMS(getContext(), json);
        result.pos = getArguments().getInt("pos");

        final Calendar start = Calendar.getInstance();
        start.setTimeInMillis(result.schedule.start);
        start.set(Calendar.HOUR_OF_DAY, result.schedule.hour);
        start.set(Calendar.MINUTE, result.schedule.min);

        next = (TextView) v.findViewById(R.id.schedule_edit_next);

        phone = (TextView) v.findViewById(R.id.schedule_edit_phone);
        message = (TextView) v.findViewById(R.id.schedule_edit_message);

        hide = (CheckBox) v.findViewById(R.id.schedule_edit_hide);

        date = (TextView) v.findViewById(R.id.schedule_edit_date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        start.set(Calendar.YEAR, year);
                        start.set(Calendar.MONTH, monthOfYear);
                        start.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        result.schedule.setTime(start.getTimeInMillis());
                        updateDate();
                    }
                }, start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });

        time = (TextView) v.findViewById(R.id.schedule_edit_time);
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog dialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        start.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        start.set(Calendar.MINUTE, minute);
                        result.schedule.setTime(start.getTimeInMillis());
                        updateDate();
                    }
                }, start.get(Calendar.HOUR_OF_DAY), start.get(Calendar.MINUTE), true);
                dialog.show();
            }
        });

        repeat = (Spinner) v.findViewById(R.id.schedule_edit_repeat);
        repeat.setAdapter(ScheduleTime.create(getContext()));
        repeat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                result.schedule.repeat = ((ScheduleTime.SpinnerItem) repeat.getAdapter().getItem(position)).id;
                result.schedule.setTime(start.getTimeInMillis());
                updateDate();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        sim = (Spinner) v.findViewById(R.id.schedule_edit_sim);

        List<SIMItem> sims = new ArrayList<>();
        sims.add(new SIMItem(getString(R.string.default_sim), -1));
        for (int i = 0; i < sm.getCount(); i++) {
            sims.add(new SIMItem("SIM" + (i + 1) + " (" + sm.getOperatorName(i) + ")", sm.getSimID(i)));
        }
        ArrayAdapter<SIMItem> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, sims);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sim.setAdapter(spinnerArrayAdapter);

        update();

        return v;
    }

    void update() {
        final Calendar start = Calendar.getInstance();
        start.setTimeInMillis(result.schedule.start);

        phone.setText(result.schedule.phone);
        message.setText(result.schedule.message);

        hide.setChecked(result.schedule.hide);

        repeat.setSelection(ScheduleTime.find(repeat.getAdapter(), result.schedule.repeat));

        if (result.schedule.sim == -1)
            sim.setSelection(0);
        else
            sim.setSelection(1 + sm.findID(result.schedule.sim));

        updateDate();
    }

    void updateDate() {
        date.setText(result.schedule.formatDate());
        time.setText(result.schedule.formatTime());
        if (result.schedule.next != 0) {
            next.setText("next: " + ScheduleTime.formatDate(result.schedule.next) + " " + ScheduleTime.formatTime(getActivity(), result.schedule.next));
            next.setVisibility(View.VISIBLE);
        } else {
            next.setVisibility(View.GONE);
        }
    }

    void save() {
        result.schedule.enabled = true;
        result.schedule.phone = phone.getText().toString();
        result.schedule.message = message.getText().toString();
        result.schedule.sim = ((SIMItem) sim.getSelectedItem()).id;
        result.schedule.hide = hide.isChecked();
        // date/time already saved
    }
}
